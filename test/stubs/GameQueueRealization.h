#include <IGameQueue.h>
#include <ICommand.h>
#include <queue>

class GameQueueRealization : public IGameQueue
{
public:
	virtual void push(ICommand* com) override
	{
		_queue.push(com);
	}

	virtual void pop() override
	{
		_queue.pop();
	}

	virtual ICommand* front() override
	{
		return _queue.front();
	}

	virtual int size()
	{
		return _queue.size();
	}

private:
	std::queue<ICommand*> _queue;
};
