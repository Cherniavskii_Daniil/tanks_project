#include <gtest/gtest.h>
#include <vector>
#include <QuadraticEquation.h>

using namespace std;

TEST(SQROOTS, OneRoot)
{
	vector<double> p_answer = solve(1, -6, 9);
	EXPECT_EQ(3, p_answer[0]);
	EXPECT_EQ(3, p_answer[1]);
}

TEST(SQROOTS, TwoRoots)
{
	vector<double> p_answer = solve(1, -8, 12);
	vector<double> correct = { 6, 2 };
	if (p_answer[0] == correct[0])
	{
		EXPECT_EQ(correct[0], p_answer[0]);
		EXPECT_EQ(correct[1], p_answer[1]);
	}
	else
	{
		EXPECT_EQ(correct[1], p_answer[0]);
		EXPECT_EQ(correct[0], p_answer[1]);
	}
}

TEST(SQROOTS, zeroA)
{
	EXPECT_THROW(solve(0, 1, 1), NonSquareEquation);
}

TEST(SQROOTS, NotRealRoots)
{
	EXPECT_THROW(solve(1, 0, 1), NotRealRoots);
}