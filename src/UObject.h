#include <unordered_map>
#include <any>
#include <string>
#include "IUObject.h"

class UObject: public IUObject
{
public:
	virtual std::any getProperty(const std::string& key) override;
	virtual void setProperty(const std::string& key, const std::any& value) override;

private:
	std::unordered_map<std::string, std::any> _store;
};
