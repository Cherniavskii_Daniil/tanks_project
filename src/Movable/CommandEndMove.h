#pragma once

#include <ICommand.h>
#include <IUObject.h>
#include <EmptyCommand.h>

class CommandEndMove : public ICommand
{
public:
	CommandEndMove(IUObject& obj) : _obj(obj)
	{}

	virtual void execute() override
	{
		_obj.setProperty("moveCommand", static_cast<ICommand*>(&_emptyCommand));
	}

private:
	IUObject& _obj;
	EmptyCommand _emptyCommand;
};
